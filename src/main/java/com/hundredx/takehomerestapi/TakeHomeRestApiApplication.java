package com.hundredx.takehomerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TakeHomeRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TakeHomeRestApiApplication.class, args);
	}

}

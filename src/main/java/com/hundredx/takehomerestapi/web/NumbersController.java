package com.hundredx.takehomerestapi.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hundredx.takehomerestapi.web.service.NumbersService;

@RestController
@RequestMapping("/v1/numbers")
public class NumbersController {

    @Autowired
    NumbersService numbersService;

    public NumbersController() {
    }

    public NumbersController(NumbersService numbersService) {
        this.numbersService = numbersService;
    }

    /**
     * List / Search functionality, that can take in multiple numbers.
     *
     * If a number in the criteria does not exist, then the count for that number is 0.
     * If no search criteria, then returns all numbers
     *
     * @param request
     * @return
     */
    @GetMapping
    SearchResponse search(SearchRequest request) {
        /*
         * Should match the JSON structure:
         *
         * {
         *   "count": 2,
         *   "data": [
         *        {
         * 	  "number": 0,
         * 	  "count": 2
         *    },
         *    {
         * 	  "number": 15,
         * 	  "count": 1
         *    }
         *   ]
         * }
         */
        return null;
    }

    /**
     * Returns Datum if number is found. Otherwise, returns 404.
     * @param number
     * @return
     */
    @GetMapping("/{number}")
    Datum find(@PathVariable Long number) {
        // See JSON structure of search API, for structure for datum
        return null;
    }

    /**
     * saves the new number, and then returns the number + the count
     * @param number
     * @return
     */
    @PostMapping
    Datum create(@RequestBody Long number) {
        return null;
    }

    /**
     * removes every instance of a number, and then return the number + count
     * @return
     */
    @DeleteMapping
    Datum delete() {
        return null;
    }
}

class SearchRequest {
    List<Long> numbers;
}

class SearchResponse {
}

class Datum {
}

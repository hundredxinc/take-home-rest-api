package com.hundredx.takehomerestapi.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.hundredx.takehomerestapi.web.service.NumbersService;

public class NumbersControllerSpec {
    NumbersService service;
    NumbersController controller;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void init() {
        service = new NumbersService();
        controller = new NumbersController(service);
    }

    // Write unit tests for search, create, and delete
    @Test
    public void find() throws JsonProcessingException {
        // After successful code implementation, this unit test, as it is right now, should pass
        // However, modify unit test. Get rid of JSON / String code, and compare actual Datum objects
        ObjectNode expected = mapper.createObjectNode();
        expected.put("number", 10);
        expected.put("count", 1);
        String expectedStr = mapper.writeValueAsString(expected);

        Datum datum = controller.find(10L);
        String datumAsJSON = mapper.writeValueAsString(datum);

        assertEquals(expectedStr, datumAsJSON);
    }
}

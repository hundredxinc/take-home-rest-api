# Some Background
The goal of this challenge is to implement a REST API that closely mimics what you will work on day-to-day at HundredX. While our actual code interacts with databases, to reduce set-up complexity, you'll be working with a text file.

Our back-end stack at HundredX is mainly Groovy / Kotlin / Java for the languages, and Spring Boot / Micronaut for the frameworks. As such, we've provided some skeleton code at https://bitbucket.org/hundredxinc/take-home-rest-api/src/master/ to be used as a starting point.

While we'd prefer that the solution you implement uses our tech stack, feel free to implement the solution using whatever technologies you prefer.

Tip: It may still be worthwhile to glance over the sample code, even if you ultimately don't use it.

# Requirements

Given a file that may return the number(s) 0 to 100, zero or more times, in no particular order, for example:

```
0
0
1
5
10
15
4
```

1. Write a REST API that implements the following:

- List / Search endpoint, that can take in multiple numbers. If a number in the criteria doesn’t exist, then the count for that number should be 0. If no search criteria, then return all numbers

The JSON structure of the response should match:

```json
{
  "count": 2,
  "data": [
	{
	  "number": 0,
	  "count": 2
	},
	{
	  "number": 15,
	  "count": 1
	}
  ]
}
```

- Find endpoint. Should return a JSON response if the number is found, and a 404 if the number is not found.

JSON response should match:

```json
{
  "number": 15,
  "count": 1
}
```

- Create endpoint. Save the new number, and then return the number + the count. For the purposes of this exercise, do NOT persist the changes back to the text file.

- Delete endpoint. Remove all instances of the number, and then return the number + count. For the purposes of this exercise, do NOT persist the changes back to the text file.



2. Write unit tests

# How to submit
Please upload the code for this project to Bitbucket or GitHub, and post a link to your repository in coderbyte.